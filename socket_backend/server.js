// connect to database using mongoose

// import package
const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const { Storage } = require("@google-cloud/storage");
const buff = require('base64-arraybuffer');


// App Initialization
const app = express();

// use cors to eliminate browser error of cors.
app.use(cors());

app.use(morgan('dev'))
// to log every request & response in console, 'dev' is for colorfull logging of request & response.
// app.use(morgan('combined', { stream: accessLogStream }));

// Reads the buffer as plain text and exposes the resulting string and parsing it to json on req.body.
app.use(bodyParser.json());

// bodyParser.urlencoded() provides middleware for automatically parsing forms with 
// the content-type application/x-www-urlencoded and storing the result as a dictionary (object) in req.body.
app.use(bodyParser.urlencoded({ extended: true }));

// Set EJS as templating engine 
app.set('view engine', 'ejs');

var http = require('http').Server(app);
var io = require('socket.io')(http);
const multer = require('multer');

// Multer is required to process file uploads and make them available via
// req.files.
const m = multer({
    storage: multer.memoryStorage(),
    // limits: {
    //   fileSize: 5 * 1024 * 1024 // no larger than 5mb
    // }
});

// projectId
const GOOGLE_CLOUD_PROJECT_ID = 'workspect';
// Path of private key.
const GOOGLE_CLOUD_KEYFILE = 'workspect.json';

// Instantiate a storage client
const googleCloudStorage = new Storage({
    projectId: GOOGLE_CLOUD_PROJECT_ID,
    keyFilename: GOOGLE_CLOUD_KEYFILE
});

// A bucket is a container for objects (files).
const bucket = googleCloudStorage.bucket('rtsp-cam');

// {useCreateIndex:true}: If true, this connection will use createIndex()
// instead of ensureIndex() for automatic index builds via Model.init().

// {useNewUrlParser:true}: Flag for using new URL string parser instead 
// of current (deprecated) one.

// { useFindAndModify: false }: MongoDB driver's findOneAndUpdate() 
// function uses the MongoDB driver's findAndModify() function which has been deprecated.
mongoose.connect("mongodb+srv://Admin:Airpix123@cluster0-0ca4d.gcp.mongodb.net/AirpixBpcl?retryWrites=true&w=majority",
    { useCreateIndex: true, useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true },
    err => {
        if (err) {
            console.log("Error while connecting the database...");
        }
        if (!err) {
            console.log("Database connected successfully")
        }
    });

const Schema = mongoose.Schema;
// camera_streams
const cameraSchema = new Schema({}, { strict: false });
const Camera = mongoose.model('camera_streams', cameraSchema, 'camera_streams');

// Geojson schema.
const livepc = mongoose.model('system_status', new Schema({
}, {
    strict: false,
    collection:'system_status'
}));

GetAllCamera = (req, res) => {
    Camera.find({ camera_id: req.params.camera_id }, (err, data) => {
        if (err) res.send(err);
        if (data) res.status(200).json({ message: "success", data })
    })
}

// Get all pc
GetAllPCs=(req, res)=>{
    livepc.find({},(err, data)=>{
        if(err) res.send(err);
        if(data){
            res.status(200).json({ message: "Success", data })
        }
    })
}

GetByLocation = (req, res) => {
    livepc.findOne({ "name": req.params.location }, (err, data) => {
        if (err) res.send(err);
        if (data) {
            res.status(200).json({ message: "Success", data })
        }
    });
}

var people={};

var edge_devices =[];
var browser = [];

io.on('connection', socket => {
    // frontend
    socket.emit('id', {"id": socket.id })

    // setInterval(function () {
    //     livepc.findOne({ "name": 'Kharghar'}, (err, data) => {
    //         if (err) res.send(err);
    //         if (data) {
    //             socket.emit('FromAPI',data)     
    //         }
    //     });
    // }, 1000); 

    socket.on('edgeinfo',(data, temp)=>{
        console.log("edge info",data);
        edge_devices.push(data);
    })

    socket.on('browserinfo',(data, temp)=>{
        console.log("browser info",data);
        browser.push(data);
    })

    socket.on('send_data_to_browser',(data, temp)=>{
        console.log("resposne from edge device to send to browser",data);
        // frontend
        console.log(data.data, ',,,,,,,,,,,,,,')
        socket.broadcast.emit('reply_from_edge',data);
    })
    // frontend
    socket.on('fetchdata',(data, temp)=>{
        console.log("fetch data info",data);
        key = null
        edge_devices.forEach((item, index)=>{
            console.log("device",item);
            if(item.mac_id == data.from){
                key = index;
            }
        })

        if (key == null){
            console.log("Device not connected");
        }
        else{
            console.log("fetch_edge_data",data)
            socket.broadcast.emit('fetch_edge_data',{
                "id":edge_devices[key].id,
                "browser":data.id,
                "type":data.type
            })
        }

        
    })

    socket.on('FromAPI',(data, temp)=>{
        console.log(data)
        people[data.name] =  socket.id;
        console.log(socket.id)
        // broadcat a event to all client
        socket.broadcast.emit(data.name, data.ip);
    })

    socket.on('data',(data, temp)=>{
        console.log(data, 'data')
        // broadcat a event to all client
        console.log(people[data.name], "tttttttttttt")
        socket.broadcast.to(people[data.name]).emit("temp", data);
        // socket.broadcast.emit('temp',data)
    })
})
http.listen(5001, function () {
    console.log('listening on *:5001');
});