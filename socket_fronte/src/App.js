import React, { useState, useEffect } from 'react';
import socketIOClient from "socket.io-client";
import './App.css';


const ENDPOINT = "http://localhost:5001";


function App() {
  const [response, setResponse] = useState([]);
  
  let socket_id = null;

  useEffect(() => {
    const socket = socketIOClient(ENDPOINT);
    socket.on("temp", data => {
      console.log(data)
      setResponse(data);
    });

    socket.on("id", data => {
      console.log("socket id",data);
      socket_id = data.id;
    });

    socket.on("reply_from_edge", data => {
      console.log("information from edge device",(data.data).split("'")[1]);
      setResponse((data.data).split("'")[1]);
      if(data.browser === socket_id){
        console.log("the data i requested", data.reply);
      }
      else{
        console.log("this was not my request")
      }
    });


  }, []);
 
  const toBase64=(arr)=> {
    //arr = new Uint8Array(arr) if it's an ArrayBuffer
    // btoa is inbuilt javascript function
    // return btoa(
    //    arr.reduce((data, byte) => data + String.fromCharCode(byte), '')
    // );
    return btoa(String.fromCharCode(...new Uint8Array(arr)));
  }

  const getData = () => {
    const socket = socketIOClient(ENDPOINT);
    // socket.emit("FromAPI", data => {
    //   setResponse(data);
    // });
    socket.emit('fetchdata', {"type":"getPic" , "from": 12345 , "id": socket_id})
  }

  return (
    <div className="App">
      <button onClick={getData}>Get details</button>
      <br />
      {/* <div>{JSON.stringify(response)}</div> */}
      <img src={`data:image/png;base64,${response}`} alt="temp" />
    </div>
  );
}

export default App;
